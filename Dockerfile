FROM ubuntu:22.04

ENV SHELL /bin/bash

RUN apt-get update && apt-get -y install python3 python3-pip \
    git-core bash-completion \
    sqlite \
    redis \
    wget

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt


############## ADD NON-ROOT DEFAULT USER ###########
# https://code.visualstudio.com/remote/advancedcontainers/add-nonroot-user
ARG USERNAME=dev
ARG USER_UID=1000
ARG USER_GID=${USER_UID}
RUN groupadd --gid ${USER_GID} ${USERNAME} && useradd --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME}

# Add sudo support
RUN apt-get update \
    && apt-get install -y sudo \
    && echo ${USERNAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USERNAME} \
    && chmod 0440 /etc/sudoers.d/${USERNAME}

# Add device support (needed to be allowed to use the flasher)
RUN usermod -a -G plugdev ${USERNAME}

# Required to be able to open /dev/tty* for e.g. picocom/putty
RUN usermod -a -G dialout ${USERNAME}

# Set the default user
USER ${USERNAME}
####################################################