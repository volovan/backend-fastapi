from fastapi import FastAPI

from src.recipe.router import recipe_router
from src.user.router import user_router
from src.auth.router import auth_router

from src.database import init_auth_database

init_auth_database()

app = FastAPI()
app.include_router(recipe_router)
app.include_router(user_router)
app.include_router(auth_router)


@app.get("/")
def home():
    return {"message": "Hello World"}
