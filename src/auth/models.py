from sqlalchemy import Column, String, Integer
from datetime import datetime

from src.database import user_auth_base

class UserAuthDB(user_auth_base):

    __tablename__ = "UsersAuth"

    user_id = Column(Integer, autoincrement=True, primary_key=True)

    email = Column(String)
    hashed_password = Column(String)
    created_on = Column(Integer)

    def __init__(self, email: str, hashed_password: str) -> None:
        super().__init__()

        self.email = email
        self.hashed_password = hashed_password
        self.created_on = datetime.utcnow().timestamp()

    def __repr__(self):
        return f"User with ID: {self.user_id}, email: {self.email}, and hashed_password: {self.hashed_password}. Created on: {datetime.fromtimestamp(self.created_on).isoformat()}"