from typing import Union

from fastapi import APIRouter, HTTPException, status, Response, Cookie, Form

from .schemas import UserAuthInput

import src.auth.service as auth_service
import src.session.service as session_service

auth_router = APIRouter(prefix="/auth", tags=["auth"])


@auth_router.post("/create-user", status_code=201, 
                                  responses={201: {"description": "User has been created."}, 
                                             400: {"description": "A user with that email already exists."}
                                            })
def create_user(new_user_auth_input: UserAuthInput):
    auth_service.add_user_to_db(email=new_user_auth_input.email, password=new_user_auth_input.password)
    return {"message": "User has been created."}


@auth_router.post("/login", status_code=200, responses={200: {"description": "Login successful."}})
def login(response: Response,
          email: str = Form(),
          password: str = Form(),
          session_id: Union[str, None] = Cookie(default=None)):

    user_auth = auth_service.get_user_auth_info(email=email)

    if not auth_service.user_credentials_match(password, user_auth.hashed_password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, 
                            detail="Incorrect username or password", 
                            headers={"WWW-Authenticate": "Bearer"})
    
     # If we already have a session id. Check if it is still valid
    if session_id:
        if session_service.session_is_valid(session_id=session_id):
            return {"message": "Already logged in."}

    # Check if there is already an old session on the db with this email
    session_id = session_service.get_session_id_from_email(email=user_auth.email)

    # Create a new one if it did not exist
    if not session_id:
        session_id = session_service.create_session(user_id=user_auth.user_id, email=user_auth.email)

    response.set_cookie(key="session_id", value=session_id, httponly=True, secure=True, samesite="strict")
    return {"message": "Login successful."}


@auth_router.post("/logout")
def logout(response: Response, 
           session_id: Union[str, None] = Cookie(default=None)):

    if session_id:
        response.delete_cookie("session_id")
        session_service.delete_session(session_id=session_id)