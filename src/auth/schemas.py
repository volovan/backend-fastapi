from pydantic import BaseModel

class UserAuthInput(BaseModel):
    email: str
    password: str

class UserAuth(BaseModel):
    user_id: int
    email: str
    hashed_password: str
    created_on: int