from sqlalchemy.orm.exc import NoResultFound

from fastapi import HTTPException

from passlib.context import CryptContext

from src.database import user_auth_sessionlocal

from .schemas import UserAuth
from .models import UserAuthDB

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_user_auth_info(email: str) -> UserAuth:
    try:
        user_auth_db_session = user_auth_sessionlocal()
        query = user_auth_db_session.query(UserAuthDB).filter(UserAuthDB.email == email).one()
        user_auth_db_session.close()

        return UserAuth(**(query.__dict__))
    except NoResultFound:
        raise HTTPException(status_code=404, detail="No user with that email exists.")

def user_credentials_match(password: str, hashed_password: str) -> bool:
    return pwd_context.verify(password, hashed_password)


def add_user_to_db(email: str, password: str) -> None:
    user_auth_db_session = user_auth_sessionlocal()

    # Check if email already exists
    query = user_auth_db_session.query(UserAuthDB).filter(UserAuthDB.email == email)
    if not user_auth_db_session.query(query.exists()).scalar():
         # Hash password. passlib will automatically generate a salt and use it for the
        # hashing and append it to the hash
        hashed_salted_password = pwd_context.hash(password)
        new_user_auth = UserAuthDB(email=email, 
                                   hashed_password=hashed_salted_password)

        user_auth_db_session.add(new_user_auth)
        user_auth_db_session.commit()
        user_auth_db_session.close()
    else:
        user_auth_db_session.close()
        raise HTTPException(status_code=400, detail="A user with that email already exists.")

