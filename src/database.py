# For Auth
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# For Session
from redis import Redis

# For document store (Recipe, etc.)
from pymongo import MongoClient


# AUTH DATABASE
# connect_args={"check_same_thread": False} only necessary for sqlite since it is single thread by default
user_auth_engine = create_engine("sqlite+pysqlite:///volovan_users_auth.db", echo=True, connect_args={"check_same_thread": False})
user_auth_sessionlocal = sessionmaker(autocommit=False, autoflush=False, bind=user_auth_engine)
user_auth_base = declarative_base()

def init_auth_database():
    user_auth_base.metadata.create_all(bind=user_auth_engine)


# SESSION DATABASE
user_session_db = Redis(host="localhost", port=6379, decode_responses=True)


# DOCUMENT DATABASE
document_db = MongoClient(host="172.17.0.3", port=27017).volovan

recipe_db = document_db.recipes
