from typing import List

from fastapi import APIRouter, Depends, HTTPException

from .schemas import Recipe, UserRecipe

import src.session.service as session_service

from src.database import recipe_db

recipe_router = APIRouter(prefix="/recipes", tags=["recipes"])


@recipe_router.get("/get-all", response_model=List[Recipe], response_model_exclude_unset=True)
def get_all_recipes(user_id: int = Depends(session_service.get_user_id_from_cookie)):
    result = list(recipe_db.find({"user_id": user_id}))
    return [Recipe(**x["recipe"]) for x in result]


@recipe_router.post("/add-recipe", status_code=201, 
                                   responses={201: {"description": "Recipe has been created."}, 
                                              400: {"description": "A recipe with that name already exists."}
                                             })
def add_recipe(recipe: Recipe, user_id: int = Depends(session_service.get_user_id_from_cookie)):

    if recipe_db.count_documents({"recipe.name": recipe.name}) > 0:
        raise HTTPException(status_code=400, detail=f"A recipe with name: {recipe.name} already exists.")

    user_recipe = UserRecipe(user_id=user_id, recipe=recipe)
    recipe_db.insert_one(user_recipe.dict(exclude_unset=True))

    return {"message": "Recipe has been created."}