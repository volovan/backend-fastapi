from pydantic import BaseModel
from typing import Optional, List

class Quantity(BaseModel):
    value: float
    unit: Optional[str]

class Ingredient(BaseModel):
    name: str
    quantity: Quantity
    method: Optional[str]

class PreparationStep(BaseModel):
    text: str
    image_url: Optional[str]
    video_url: Optional[str]
    gif_url: Optional[str]

class Recipe(BaseModel):
    name: str
    ingredients: List[Ingredient]
    preparation_steps: List[PreparationStep]
    portions: Optional[int]

class UserRecipe(BaseModel):
    user_id: int
    recipe: Recipe