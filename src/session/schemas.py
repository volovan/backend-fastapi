from pydantic import BaseModel

class SessionData(BaseModel):
    user_id: str
    email: str
    created_on: int