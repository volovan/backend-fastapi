from datetime import datetime
from typing import Union

from passlib import pwd
from fastapi import Cookie, HTTPException, status

from src.database import user_session_db

from .schemas import SessionData

def get_session_data(session_id: str) -> SessionData:
    
    # If not found will be None
    session_data = user_session_db.hgetall(session_id)
    if len(session_data.keys()) > 0:
        return session_data
    else:
        return None

def get_session_id_from_email(email: str) -> str:
    session_id = user_session_db.get(email)

    return session_id

def get_user_id_from_session_id(session_id: str) -> int:
    return int(user_session_db.hget(name=session_id, key="user_id"))

def get_user_id_from_cookie(session_id: Union[str, None] = Cookie(default=None)) -> bool:
    if not session_id or not session_is_valid(session_id=session_id):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="User is not logged in.")
    else:
        return get_user_id_from_session_id(session_id=session_id)

def get_session_id_from_cookie(session_id: Union[str, None] = Cookie(default=None)) -> str:
    if not session_id or not session_is_valid(session_id=session_id):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="User is not logged in.")
    else:
        return session_id

def session_is_valid(session_id: str) -> bool:
    return user_session_db.exists(session_id)

def create_session(user_id: int, email: str) -> str:

    session_id = ""
    while True:
        session_id = pwd.genword(entropy=64, length=24, charset="ascii_72")

        if not user_session_db.hgetall(session_id):
            break
    
    # Create a hashmap with all session info
    user_session_db.hmset(name=session_id, 
                          mapping=SessionData(user_id=user_id, 
                                              email=email, 
                                              created_on=datetime.utcnow().timestamp()).dict())

    # Create a pair of email and session_id for easy retrieval of session based on email
    user_session_db.set(email, session_id)
    
    return session_id

def delete_session(session_id: str) -> None:
    session_email = user_session_db.hget(name=session_id, key="email")
    
    user_session_db.delete(session_email)
    user_session_db.delete(session_id)