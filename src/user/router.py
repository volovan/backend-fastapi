from fastapi import APIRouter, Depends

import src.session.service as session_service
from src.session.schemas import SessionData

user_router = APIRouter(prefix="/users", tags=["users"])


@user_router.get("/me", response_model=SessionData)
async def read_users_me(session_id: str = Depends(session_service.get_session_id_from_cookie)):

    session_data = session_service.get_session_data(session_id=session_id)

    return session_data
